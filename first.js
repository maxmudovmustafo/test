fun sayHello() {
	println("Hello world!")
}

fun sayHi() {
	println("Hi Jems!")
}

fun main() {
	sayHello()
	sayHi()
}